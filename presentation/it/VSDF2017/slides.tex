%
% Copyright (C) 2017 Nicola Bernardini nicb@sme-ccppd.org
% 
% This work is licensed under a Creative Commons License, and specifically the
% 
%   Creative Commons Attribution-ShareAlike 2.5 License
%   http://creativecommons.org/licenses/by-sa/2.5/legalcode
% 
% Check http://www.creativecommons.org/ for more information on
% Creative Commons Licenses and the Creative Commons Project.
%
% Title: ``Singolare/Plurale: la vocalit\`a nella musica contemporanea''
% 
% Abstract (italiano)
% 
% Come molti altri strumenti, anche la voce cantata e la vocalit\`a sono radicalmente cambiate nel
% XX secolo. Tali mutamenti sono stati determinati dalle necessit\`a espressive
% dei compositori. Questo processo ha poi portato alla disintegrazione del
% concetto di vocalit\`a
% conducendoci verso una visione \emph{plurale} della voce
% stessa. Questa breve presentazione riassumer\`a alcune tra le principali
% tendenze in ambito contemporaneo in questo contesto. 
% 
% Abstract (english)
% 
% The singing voice has undergone radical changes during the XX
% century, following the fate of many acoustical instruments.
% These changes have often followed the expressive needs of composers,
% leading to its shattering into many different instruments
% promoting a plural vision of the voice itself. This presentation will give a
% broad overview of its capabilities and possibilities.
%
% Set the macros below to whatever is appropriate in a given context
%

\newcommand{\imagedir}{../../../materials}
\newcommand{\exampledir}{../../../materials}
\newcommand{\templatedir}{../../../../latex/beamer/smerm}
\documentclass[compress]{beamer}

\usepackage{beamerthemeSMERM}
\usepackage{beamercolorthemeSMERM}
\usepackage{beamerinnerthemeSMERM}

\usepackage{colortbl}
\usepackage[italian]{babel}
\usepackage{pgf}
\usepackage{xspace}

\usepackage{multimedia}
\usepackage{xmpmulti}
\usepackage{hyperref}
\usepackage{gitinfo2}
\newcommand{\rcstag}{rev.\gitAbbrevHash\ \ \gitAuthorDate\xspace}
\usepackage{gensymb}

\pgfdeclareimage[width=16pt]{speaker}{\imagedir/speaker}
\newcommand{\esdplay}[2]{\href{run: esdplay \exampledir/#1}{#2 \pgfuseimage{speaker}}}
\newcommand{\oggplay}[2]{\href{run: ogg123 -d esd \exampledir/#1}{#2 \pgfuseimage{speaker}}}

\newcommand{\cpyear}{2017}
\newcommand{\cpholder}{Nicola Bernardini}
\newcommand{\cpholderemail}{n.bernardini@conservatoriosantacecilia.it}

% Use some nice templates

%\beamertemplateshadingbackground{red!10}{structure!10}
\beamertemplatetransparentcovereddynamic
\beamertemplateballitem
\beamertemplatenumberedballsectiontoc

% My colors
\definecolor{notdone}{gray}{0.35}

%\usecolortheme[named=MyColor]{structure}
%\usecolortheme[named=MyColor]{structure}
\beamertemplateshadingbackground{white!10}{white!10}

\input{\templatedir/macros}

\title[Singolare/Plurale]
{%
  {\large Singolare/Plurale: la vocalit\`a nella musica contemporanea}\\
  {\tiny (\rcstag)}
}

\author{%
  Nicola Bernardini\\
    \href{mailto:\cpholderemail}{\cpholderemail}
}
\institute[SMERM]%
{%
  \href{http://www.conservatoriosantacecilia.it}
     {Conservatorio di Musica ``S.Cecilia'' -- Roma}
}
\date[VSDF2017 Padova, 13/10/2017]{Padova, Convegno ``Voci e Suoni di Dentro e di Fuori'' -- 13/10/2017}

\begin{document}
\newcounter{ms}
  
\begin{frame}
  \titlepage
\end{frame}

\section{Introduzione}

\begin{frame}
  \frametitle<+->{Contesto Storico}

  \begin{enumerate}[<+- | alert@+>]

     \item Inizio '900

     \item Crollo del mondo romantico

     \item Transizione verso la seconda era industriale

     \item Rivoluzioni tecnologiche \uncover<+->{(tra le quali la \emph{riproducibilit\`a dei suoni})}

     \item Progresso esponenziale

  \end{enumerate}

\end{frame}

\begin{frame}
  \frametitle<+->{Contesto Musicale}

  \begin{enumerate}[<+- | alert@+>]

     \item Crisi dei linguaggi classici e romantici

     \item Nascita dell'industria musicale

     \item Emancipazione del timbro come componente musicale primaria

     \item Divaricazione delle funzioni musicali

  \end{enumerate}

\end{frame}

\begin{frame}
  \frametitle<+->{Ricerca Compositiva}

  \begin{enumerate}[<+- | alert@+>]

     \item Numerose direzioni diverse

     \item Di particolare interesse: la \emph{connotazione} musicale come
             elemento compositivo primario

     \item In questo contesto, la \emph{voce} gioca un ruolo fondamentale

  \end{enumerate}

\end{frame}

\section{Quattro esempi}

\begin{frame}
   \vspace{0.15\textheight}
   \begin{center}
      \uncover<+->{\huge Quattro esempi musicali}
   \end{center}

\end{frame}

\subsection{Sch\"onberg}

\begin{frame}

   \frametitle<+->{Arnold Sch\"onberg, \emph{Pierrot Lunaire}, op.21 (1912)}

   \newcommand{\schoenbergfragment}{Arnold Sch\"onberg, \emph{Pierrot Lunaire}, op.21 -- n.7 ``Der Kranke Mond'', batt.1--8\xspace}
   \begin{center}
     \begin{figure}[htbp]
       \includegraphics[width=0.75\textwidth]{\imagedir/Schoenberg_Pierrot_7_first_page}
       \caption%
       {
         \begin{footnotesize}
         \schoenbergfragment 
         \listento{run:\exampledir/Schoenberg_Pierrot_7_bars_1_8.mp4}{\url{https://www.youtube.com/watch?v=Ykh4RUBYinE}}
         \end{footnotesize}
       }
     \end{figure}
   \end{center}
\end{frame}

\subsection{Berio}

\begin{frame}

   \frametitle<+->{Luciano Berio, \emph{Sequenza III} per voce (1966)}

   \newcommand{\beriofragment}{Luciano Berio, \emph{Sequenza III} -- voce: Cathy Berberian -- primi trenta secondi ca.\xspace}
   \begin{center}
     \begin{figure}[htbp]
       \includegraphics[width=0.75\textwidth]{\imagedir/Berio_Sequenza_III-first_page}
       \caption%
       {
         \beriofragment
         \listento{run:\exampledir/Berio_Sequenza_III-primi_trenta_secondi.mkv}{\url{https://www.youtube.com/watch?v=DGovCafPQAE}}
       }
     \end{figure}
   \end{center}
\end{frame}

\subsection{Bernardini}

\begin{frame}

   \frametitle<+->{Nicola Bernardini, \emph{Partita} per voce (1985)}

   \newcommand{\bernardinifragment}{Nicola Bernardini, \emph{Partita} -- voce: Leonore Colbert --  trenta secondi finali\xspace}
   \begin{center}
      \listento{run:\exampledir/Partita_Lee_Colbert_1994-ultimi_trentacinque_secondi.mp3}{\footnotesize \bernardinifragment}\linebreak
      {\footnotesize \url{https://soundcloud.com/nicb-4/partita-per-voce-sola-1985}}
   \end{center}

\end{frame}

\subsection{Ligeti}

\begin{frame}

  \frametitle<+->{Gy\"orgy Ligeti, \emph{Mysteries of the Macabre}, per sop. di coloratura e piccola orch. (1992)}

   \newcommand{\ligetifragment}{Gy\"orgy Ligeti, \emph{Mysteries of the Macabre}, Barbara Hannigan -- Simon Rattle -- London Symphony Orchestra}
   \begin{center}
     \begin{figure}[htbp]
       \includegraphics[width=0.55\textwidth]{\imagedir/Ligeti_Mistere_du_Macabre-Rattle-Hannigan-London_SyO}
       \caption%
       {
           \ligetifragment\linebreak
           \listento{run:\exampledir/Ligeti_Mistere_du_Macabre-Rattle-Hannigan-London_SyO-first_1_30.mp4}{\url{https://www.youtube.com/watch?v=w0Tvj83xqDw}}
       }
     \end{figure}
   \end{center}
\end{frame}

\end{document}
