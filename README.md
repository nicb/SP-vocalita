# *Singolare/Plurale*: la vocalità nella musica contemporanea

Speech (in italian) delivered:

1. during the *Voci e suoni di dentro e di fuori* Conference Padova, October 12-14 2017.

## LICENSE

<img
  src=http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png
  width=88
  alt="CreativeCommons Attribution-ShareAlike 4.0"
/>
